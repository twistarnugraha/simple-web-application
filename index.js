const express = require('express')
const requestIp = require('request-ip');
const app = express()
const port = 3000

app.get('/', (req, res) => {
    const clientIp = requestIp.getClientIp(req); 
    res.send(clientIp);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})